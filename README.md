# MAC216-EP4

3D Tic Tac Toe using Pygame | MAC216 - IME-USP

## Authors:

- 4876102  | [Arthur Teixeira Magalhães](amarthur@usp.br)
- 10788620 | [Giovani Tavares de Andrade](giovanitavares@usp.br)
- 11796378 | [João Henri Carrenho Rocha](joao.henri@usp.br)
## Dependencies

In this project, we used [pygame](https://www.pygame.org) and [pytest](http://pytest.org/).

### Versions:

- Python's version used was [3.7.6](https://www.python.org/downloads/release/python-376/)
- pygame [2.0.0](https://pypi.org/project/pygame/)
- pytest [6.1.2](https://docs.pytest.org/en/stable/announce/release-6.1.2.html)

## Running the project

Firstly, make sure to have the dependencies mentioned above. To run, in the project's folder, execute:
```
python app/main.py
```
or, alternatively:
```
python3 app/main.py
```

## Running tests

In order to run the tests, you **MUST** be under the **tests** directory. Then, execute:
```
pytest
```
## How To Play

First, select your parameters from the menu screen using the **Arrow Keys** and press **ENTER** to confirm.

During the game, you can press **ESC** at any time to return to the main menu.

When the game is over, you may use it again or press **ENTER** to restart with the same players.


## Project Structure

We are following Kenneth Reitz's guide, renowned Python Software Foundation member and open source developer:

- https://docs.python-guide.org/writing/structure/
- https://github.com/navdeep-G/samplemod/

## Project Architecture

![Clean Arch's diagram](docs/cleanarch.jpg)

In this project, we used [Robert C. Martin](http://cleancoder.com)'s [Clean Arch](https://www.amazon.com/dp/0134494164) design.

We modularized the application code in 3 main modules and 1 exception folder.

#### Entities
Under the **entities** folder lies the project's main Objects, which the **use cases** interact with. Ideally, there is not much *Business Logic* and library/framework dependencies inside these classes.

#### Use Cases
On the other hand, the **use case** folder has the responsible of keeping *business logic* and interacting with entities. For example, unlike a Player, that has an ID, the *paths_checker* does not have own properties, its only responsibility is to iterate over a received board. The *board_inserter*, as the name says, only inserts players in a given board, but has no properties. They are utilitary classes, however they hold no object.

#### Gateways
Lastly, there is the **gateway** package. The gateway is the most *external* package. The gateway is responsible for interacting with frameworks, reducing the coupling between *business rules* and *dependencies*. 

#### Why Clean Arch?
This way, it is easier to test the project's features sololy, avoiding frameworks, runtimes and libraries noise. Also, if necessary, Clean Arch makes it easier to change between external dependencies. For example, this project was originally made to work with **STDIN** and **STDOUT**. However, as there wasn't much coupling between the "Gateways/Presenters" and the "Entities/Use Cases", as described in the diagram above, it was easy to switch from these entries to **pygame**. 
