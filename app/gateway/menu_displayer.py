from gateway.board_displayer import BoardDisplay
import pygame, random, sys

class MenuDisplay:

    def __init__(self):
        pygame.init()
        self.board = BoardDisplay()
        self.player1 = self.player2 = 0 # Human x Human

        self.playRectangle = (self.board.screen_width/3, self.board.screen_height/2.3, self.board.screen_width/3.4, self.board.screen_height/10)
        self.quitRectangle = (self.playRectangle[0], self.board.screen_height/1.5, self.board.screen_width/3.4, self.board.screen_height/10)
        self.humanRectangle = [self.playRectangle[0],50,155,40]
        self.rawEaterRectangle = [self.playRectangle[0],self.humanRectangle[1]+75,230,self.humanRectangle[3]]
        self.confusedRectangle = [self.playRectangle[0],self.humanRectangle[1]+150,215,self.humanRectangle[3]]

        self.playButton = pygame.Rect(self.playRectangle)
        self.quitButton = pygame.Rect(self.quitRectangle)
        self.humanButton = pygame.Rect(self.humanRectangle)
        self.rawEaterButton = pygame.Rect(self.rawEaterRectangle)
        self.confusedButton = pygame.Rect(self.confusedRectangle)

        self.menuFont = pygame.font.SysFont("Times New Roman", 75)
        self.menuSubFont = pygame.font.SysFont("Times New Roman", 40)
        self.menuWinnerFont = pygame.font.SysFont("Times New Roman", 30)
        self.fontColor = [0,0,0]
        self.winnerFontColor = [255,255,255]
        self.buttonColor = self.board.SURFACE_COLOR
        self.pressedButtonColor = [125,0,125]
        self.selectPlayerColor = [85,0,85]

    def main_menu(self):
        self.board.surface.fill(self.board.SURFACE_COLOR)
        self.draw_buttons()

        if self.player1 == 0:
            self.highlight_player(self.humanButton)
            player1Label = self.menuSubFont.render('1', True, self.fontColor)
            self.board.surface.blit(player1Label, (self.humanRectangle[0]-40,self.humanRectangle[1]))
        elif self.player1 == 1:
            self.highlight_player(self.rawEaterButton)
            player1Label = self.menuSubFont.render('1', True, self.fontColor)
            self.board.surface.blit(player1Label, (self.rawEaterRectangle[0]-40,self.rawEaterRectangle[1]))
        elif self.player1 == 2:
            self.highlight_player(self.confusedButton)
            player1Label = self.menuSubFont.render('1', True, self.fontColor)
            self.board.surface.blit(player1Label, (self.confusedRectangle[0]-40,self.confusedRectangle[1]))

        if self.player2 == 0:
            self.highlight_player(self.humanButton)
            player1Label = self.menuSubFont.render('2', True, self.fontColor)
            self.board.surface.blit(player1Label, (self.humanRectangle[0]+self.humanRectangle[2]+20,self.humanRectangle[1]))
        elif self.player2 == 1:
            self.highlight_player(self.rawEaterButton)
            player1Label = self.menuSubFont.render('2', True, self.fontColor)
            self.board.surface.blit(player1Label, (self.rawEaterRectangle[0]+self.rawEaterRectangle[2]+20,self.rawEaterRectangle[1]))
        elif self.player2 == 2:
            self.highlight_player(self.confusedButton)
            player1Label = self.menuSubFont.render('2', True, self.fontColor)
            self.board.surface.blit(player1Label, (self.confusedRectangle[0]+self.confusedRectangle[2]+20,self.confusedRectangle[1]))

        self.highlight_button(self.player2)
        pygame.display.update() # Redraw main menu

    def draw_buttons(self):
        pygame.draw.rect(self.board.surface, self.buttonColor, self.playButton)
        playButt = self.menuFont.render('PLAY', True, self.fontColor)
        self.board.surface.blit(playButt, (self.playRectangle[0], self.playRectangle[1]))

        pygame.draw.rect(self.board.surface, self.buttonColor, self.quitButton)
        quitButt = self.menuFont.render('QUIT', True, self.fontColor)
        self.board.surface.blit(quitButt, (self.quitRectangle[0], self.quitRectangle[1]))

        pygame.draw.rect(self.board.surface, self.buttonColor, self.humanButton)
        humanButt = self.menuSubFont.render('HUMAN', True, self.fontColor)
        self.board.surface.blit(humanButt, (self.humanRectangle[0], self.humanRectangle[1]))

        pygame.draw.rect(self.board.surface, self.buttonColor, self.rawEaterButton)
        rawEaterButt = self.menuSubFont.render('RAW EATER', True, self.fontColor)
        self.board.surface.blit(rawEaterButt, (self.rawEaterRectangle[0], self.rawEaterRectangle[1]))

        pygame.draw.rect(self.board.surface, self.buttonColor, self.confusedButton)
        confusedButt = self.menuSubFont.render('CONFUSED', True, self.fontColor)
        self.board.surface.blit(confusedButt, (self.confusedRectangle[0], self.confusedRectangle[1]))

        pygame.display.update()

    def highlight_button(self, button):
        if button == self.playButton:
            pygame.draw.rect(self.board.surface, self.pressedButtonColor, self.playButton)
            img =self.menuFont.render('PLAY', True, self.fontColor)
            self.board.surface.blit(img, (self.playRectangle[0], self.playRectangle[1]))
        elif button == self.quitButton:
            pygame.draw.rect(self.board.surface, self.pressedButtonColor, self.quitButton)
            img =self.menuFont.render('QUIT', True, self.fontColor)
            self.board.surface.blit(img, (self.quitRectangle[0], self.quitRectangle[1]))
        elif button == self.humanButton:
            pygame.draw.rect(self.board.surface, self.pressedButtonColor, self.humanButton)
            humanButt = self.menuSubFont.render('HUMAN', True, self.fontColor)
            self.board.surface.blit(humanButt, (self.humanRectangle[0], self.humanRectangle[1]))
        elif button == self.rawEaterButton:
            pygame.draw.rect(self.board.surface, self.pressedButtonColor, self.rawEaterButton)
            rawEaterButt = self.menuSubFont.render('RAW EATER', True, self.fontColor)
            self.board.surface.blit(rawEaterButt, (self.rawEaterRectangle[0], self.rawEaterRectangle[1]))
        elif button == self.confusedButton:
            pygame.draw.rect(self.board.surface, self.pressedButtonColor, self.confusedButton)
            confusedButt = self.menuSubFont.render('CONFUSED', True, self.fontColor)
            self.board.surface.blit(confusedButt, (self.confusedRectangle[0], self.confusedRectangle[1]))

        pygame.display.update()

    def highlight_player(self, button):
        if button == self.humanButton:
            pygame.draw.rect(self.board.surface, self.selectPlayerColor, self.humanButton)
            humanButt = self.menuSubFont.render('HUMAN', True, self.fontColor)
            self.board.surface.blit(humanButt, (self.humanRectangle[0], self.humanRectangle[1]))
        elif button == self.rawEaterButton:
            pygame.draw.rect(self.board.surface, self.selectPlayerColor, self.rawEaterButton)
            rawEaterButt = self.menuSubFont.render('RAW EATER', True, self.fontColor)
            self.board.surface.blit(rawEaterButt, (self.rawEaterRectangle[0], self.rawEaterRectangle[1]))
        elif button == self.confusedButton:
            pygame.draw.rect(self.board.surface, self.selectPlayerColor, self.confusedButton)
            confusedButt = self.menuSubFont.render('CONFUSED', True, self.fontColor)
            self.board.surface.blit(confusedButt, (self.confusedRectangle[0], self.confusedRectangle[1]))

    def winner_screen(self, winner_id):
        messageSelector = random.randint(0,99) % 3

        pygame.display.update()
        if winner_id == 0:
            tieMessage = self.menuWinnerFont.render('(╯°□°)╯┻━┻ TIE!', True, self.winnerFontColor)
            self.board.surface.blit(tieMessage, (20, self.board.screen_height-100))
        elif winner_id == 1:
            if messageSelector == 0:
                winnerMessage = self.menuWinnerFont.render('Ultra brain moves Player 1!', True, self.winnerFontColor)
                winnerMessage2 = self.menuWinnerFont.render('CONGRATS!', True, self.winnerFontColor)
            elif messageSelector == 1:
                winnerMessage = self.menuWinnerFont.render('CONGRATULAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAATIONS', True, self.winnerFontColor)
                winnerMessage2 = self.menuWinnerFont.render('PLAYER 1!', True, self.winnerFontColor)
            elif messageSelector == 2:
                winnerMessage = self.menuWinnerFont.render('I am impressed with your skills, Player 1!', True, self.winnerFontColor)
                winnerMessage2 = self.menuWinnerFont.render('', True, self.winnerFontColor)

            self.board.surface.blit(winnerMessage, (20, self.board.screen_height-100))
            self.board.surface.blit(winnerMessage2, (20, self.board.screen_height-50))

        elif winner_id == 2:
            if messageSelector == 0:
                winnerMessage = self.menuWinnerFont.render('Player 2... Deep inside my heart, I always knew', True, self.winnerFontColor)
                winnerMessage2 = self.menuWinnerFont.render('that you were born to stand out.', True, self.winnerFontColor)
            elif messageSelector == 1:
                winnerMessage = self.menuWinnerFont.render('It would have been an injustice if such a brilliant', True, self.winnerFontColor)
                winnerMessage2 = self.menuWinnerFont.render('person like you wasn’t rewarded, Player 2!', True, self.winnerFontColor)
            elif messageSelector == 2:
                winnerMessage = self.menuWinnerFont.render('Player 2, all the sacrifices and sleepless ', True, self.winnerFontColor)
                winnerMessage2 = self.menuWinnerFont.render('nights have finally paid off!', True, self.winnerFontColor)

            self.board.surface.blit(winnerMessage, (20, self.board.screen_height-100))
            self.board.surface.blit(winnerMessage2, (20, self.board.screen_height-50))

    @staticmethod
    def start_menu(menu):
        upKeyCounter = 1
        playerCounter = 0

        while True: # Menu select buttons
            pygame.display.update()

            if upKeyCounter == 1:
                menu.highlight_button(menu.playButton)

            for event in pygame.event.get():

                if event.type == pygame.KEYDOWN:

                    if event.key == pygame.K_ESCAPE:
                        pygame.quit()
                        sys.exit()

                    if event.key == pygame.K_UP:

                        upKeyCounter += 1
                        menu.main_menu() # Redraw basic menu

                        if upKeyCounter == 2:
                            menu.highlight_button(menu.confusedButton)

                        elif upKeyCounter == 3:
                            menu.highlight_button(menu.rawEaterButton)

                        elif upKeyCounter == 4:
                            upKeyCounter = 4
                            menu.highlight_button(menu.humanButton)

                        elif upKeyCounter > 4:
                            upKeyCounter = 0
                            menu.highlight_button(menu.quitButton)

                    if event.key == pygame.K_DOWN:

                        upKeyCounter -= 1
                        menu.main_menu()

                        if upKeyCounter < 0:
                            upKeyCounter = 4
                            menu.highlight_button(menu.humanButton)
                        elif upKeyCounter == 0:
                            menu.highlight_button(menu.quitButton)
                        elif upKeyCounter == 1:
                            menu.highlight_button(menu.playButton)
                        elif upKeyCounter == 2:
                            menu.highlight_button(menu.confusedButton)
                        elif upKeyCounter == 3:
                            menu.highlight_button(menu.rawEaterButton)
                        elif upKeyCounter == 4:
                            menu.highlight_button(menu.humanButton)

                    if event.key == pygame.K_RETURN:

                        if upKeyCounter == 0: # Quit button
                            pygame.quit()
                            sys.exit()

                        elif upKeyCounter == 1: # Play button
                            upKeyCounter = 1
                            playerCounter = 0
                            return (menu.player1, menu.player2)

                        elif upKeyCounter == 2: # Confused Player
                            if playerCounter % 2 == 0 and menu.player1 != 2:
                                menu.player1 = 2
                            else:
                                menu.player2 = 2
                            playerCounter += 1

                        elif upKeyCounter == 3: # Raw Eater Player
                            if playerCounter % 2 == 0 and menu.player1 != 1:
                                menu.player1 = 1
                            else:
                                menu.player2 = 1
                            playerCounter += 1

                        elif upKeyCounter >= 4: # Human Player
                            upKeyCounter = 4
                            if playerCounter % 2 == 0 and  menu.player1 != 0:
                                menu.player1 = 0
                            else:
                                menu.player2 = 0
                            playerCounter += 1

                        menu.main_menu()

                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()
