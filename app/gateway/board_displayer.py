from entity.cell import Cell
import pygame, random, sys

#           0  1  2 3
#         ------------ >>> X AXIS (Horizontal)
#      0 /          /
#     1 /  BOARD   / >>> Y AXIS (Depth)
#    2 /          /
#   3 /          /
#    ----------- (Z = 0)
#
#           |
#           | >> Z AXIS (Vertical)
#           |

#           0  1  2 3
#         ------------ >>> X AXIS (Horizontal)
#      0 /          /
#     1 /  BOARD   / >>> Y AXIS (Depth)
#    2 /          /
#   3 /          /
#    ----------- (Z = 1)

class BoardDisplay:

    def __init__(self):
        pygame.init()
        point1 = 200 # Default: 200
        point2 = 350 # Default: 350
        point3 = 75  # Default: 75

        # Define general parameters
        self.tilt = 1 # Default: 1
        self.dimension = 4 # Default: 4
        self.verticalDistance = 150 # Default: 150
        self.circlePlayer = 1

        # Define board corners
        self.bottomLeft = (point1, point1)
        self.bottomRight = (point2, point1)
        self.topLeft = (point1 + self.tilt * point3, point3)
        self.topRight = (point2 + self.tilt * point3, point3)

        # Define colors
        self.SURFACE_COLOR = [50,0,50]
        self.X_HIGHLIGHT_COLOR = None
        self.CIRCLE_HIGHLIGHT_COLOR = None
        self.BOARD_COLOR = None
        self.X_COLOR = None
        self.CIRCLE_COLOR = None
        self.randomize_colors()

        # Create surface
        self.screen_width = 650
        self.screen_height = 800
        self.boardFont = pygame.font.SysFont("Times New Roman", 50)
        self.surface = pygame.display.set_mode((self.screen_width,self.screen_height)) # Default: (650,775)
        self.surface.fill(self.SURFACE_COLOR)
        pygame.display.set_caption("3D TIC TAC TOE!")

    def draw_board(self):
        for i in range(0, self.dimension): # Create n (=dimension) copies vertically
            verticalOffset = i * self.verticalDistance

            # Define the initial points of the slanted lines
            initialXPoint = self.bottomLeft[0]
            finalXPoint = self.topLeft[0]
            initialYPoint = self.bottomLeft[1] + verticalOffset
            finalYPoint = self.topLeft[1] + verticalOffset

            # Draw the slanted lines
            for j in range(0, self.dimension + 1):
                horizontalFraction = j * (self.bottomRight[0]-self.bottomLeft[0]) / self.dimension

                pygame.draw.aaline(self.surface, self.BOARD_COLOR,
                    (initialXPoint + horizontalFraction, initialYPoint),
                    (finalXPoint + horizontalFraction, finalYPoint))

            # Define the initial points of the horizontal lines
            initialXPoint = self.bottomLeft[0]
            finalXPoint = self.bottomRight[0]
            initialYPoint = finalYPoint = self.bottomLeft[1] + verticalOffset

            # Draw the horizontal lines
            for j in range(0, self.dimension + 1):
                verticalFraction = j * (self.bottomLeft[1]-self.topLeft[1]) / self.dimension
                horizontalFraction = j * (self.topLeft[0]-self.bottomLeft[0]) / self.dimension

                pygame.draw.aaline(self.surface, self.BOARD_COLOR,
                    (initialXPoint + horizontalFraction, initialYPoint - verticalFraction),
                    (finalXPoint + horizontalFraction, finalYPoint - verticalFraction))
        
        playerLabel = self.boardFont.render('Player:', True, self.BOARD_COLOR)
        self.surface.blit(playerLabel, (0,0))

    def draw_move(self, coord, player_id, pressedEnter = True, clearCenterDot = False):
        x = coord[0]
        y = coord[1]
        z = coord[2]
        verticalFraction = (self.bottomLeft[1] - self.topLeft[1]) / self.dimension
        horizontalFraction = (self.bottomRight[0] - self.bottomLeft[0]) / self.dimension

        if self.tilt == 0:
            slopeTangent = 100 # Infinite
        else:
            slopeTangent = (self.bottomRight[1] - self.topRight[1])/(self.topRight[0] - self.bottomRight[0])

        # Calculate the center of the cell
        cellCenterY = self.topLeft[1] + (y * verticalFraction) + (verticalFraction / 2)
        x1 = self.bottomLeft[0] + (self.bottomLeft[0] - cellCenterY) / slopeTangent
        cellCenterX = x1 + (x * horizontalFraction) + (horizontalFraction / 2)
        cellCenterZ = z * (self.verticalDistance)

        if (player_id == self.circlePlayer):
            if not pressedEnter:
                if clearCenterDot:
                    pygame.draw.circle(self.surface, self.SURFACE_COLOR, (cellCenterX, cellCenterY + cellCenterZ), 1.5, 0)
                else:
                    pygame.draw.circle(self.surface, (255,255,255), (cellCenterX, cellCenterY + cellCenterZ), 1.5, 0)
            else: # Circle
                for _ in range(0,3): # Blink
                    pygame.draw.circle(self.surface, self.SURFACE_COLOR, (cellCenterX, cellCenterY + cellCenterZ), horizontalFraction/3, 2)
                    pygame.display.update(); pygame.time.delay(100)
                    pygame.draw.circle(self.surface, self.CIRCLE_COLOR, (cellCenterX, cellCenterY + cellCenterZ), horizontalFraction/3, 2)
                    pygame.display.update(); pygame.time.delay(100)
        else:
            if not pressedEnter:
                if clearCenterDot:
                    pygame.draw.circle(self.surface, self.SURFACE_COLOR, (cellCenterX, cellCenterY + cellCenterZ), 1.5, 0)
                else:
                    pygame.draw.circle(self.surface, (255,255,255), (cellCenterX, cellCenterY + cellCenterZ), 1.5, 0)
            else: # X
                auxColor = self.X_COLOR
                for i in range(0,6):
                    cellTopRightX = cellCenterX + horizontalFraction/2 + (verticalFraction/(2*slopeTangent))
                    cellBottomLeftX = cellCenterX - horizontalFraction/2 - (verticalFraction/(2*slopeTangent))
                    cellTopLeftX = cellTopRightX - horizontalFraction
                    cellBottomRightX = cellBottomLeftX + horizontalFraction

                    if (i % 2): # Blink
                        self.X_COLOR = auxColor
                    else:
                        self.X_COLOR = self.SURFACE_COLOR
                    for _ in range (0,10): # Draw multiples times to make the lines more visible
                        pygame.draw.aaline(self.surface, self.X_COLOR,
                            (cellBottomLeftX, cellCenterY + verticalFraction/2 + cellCenterZ),
                            (cellTopRightX, cellCenterY - verticalFraction/2 + cellCenterZ))
                        pygame.draw.aaline(self.surface, self.X_COLOR,
                            (cellBottomRightX, cellCenterY + verticalFraction/2 + cellCenterZ),
                            (cellTopLeftX, cellCenterY - verticalFraction/2 + cellCenterZ))
                    pygame.display.update(); pygame.time.delay(100)

    def highlight_move(self, coord, player_id):
        if player_id == self.circlePlayer:
            self.CIRCLE_COLOR = self.CIRCLE_HIGHLIGHT_COLOR
            self.draw_move(coord, player_id, True, False)
        else:
            self.X_COLOR = self.X_HIGHLIGHT_COLOR
            self.draw_move(coord, player_id, True, False)

    def randomize_colors(self):
        self.BOARD_COLOR = [random.randint(110,210),random.randint(110,210),random.randint(110,210)]
        self.X_COLOR = [random.randint(110,210),random.randint(110,210),random.randint(110,210)]
        self.CIRCLE_COLOR = [random.randint(110,210),random.randint(110,210),random.randint(110,210)]
        self.CIRCLE_HIGHLIGHT_COLOR = [random.randint(110,210),0,random.randint(50,150)]
        self.X_HIGHLIGHT_COLOR = [0, random.randint(110,210),random.randint(50,150)]

    def reset_screen(self):
        radius = 1
        while radius <= max(self.screen_width, self.screen_height):
            rainbow = [random.randint(0,150),0,random.randint(0,200)]
            pygame.draw.circle(self.surface, rainbow, (self.screen_width/2, self.screen_height/2), radius, 2)
            radius += 0.65
            pygame.display.update() # Cool effect

        radius = 1
        while radius <= max(self.screen_width, self.screen_height):
            pygame.draw.circle(self.surface, self.SURFACE_COLOR, (self.screen_width/2, self.screen_height/2), radius, 0)
            radius *= 1.2
            pygame.time.delay(25)
            pygame.display.update() # Reset the screen to its original color

        # Reset
        self.randomize_colors()

    def update_player_display(self, player_id):
        pygame.draw.rect(self.surface, self.SURFACE_COLOR, (150,0,50,50)); pygame.display.update()
            
        if player_id == self.circlePlayer:
            currentPlayerLabel = self.boardFont.render('O', True, self.CIRCLE_COLOR)
        else:
            currentPlayerLabel = self.boardFont.render('X', True, self.X_COLOR)
                
        self.surface.blit(currentPlayerLabel, (150,0))

        pygame.display.update()

    def get_player_choice(self, player_id, board):
        coord = board.get_free_coord()

        pressedEnter = False
        while not pressedEnter:

            self.draw_move(coord, player_id, False) # Indicates current position of the player
            pygame.display.update()

            for event in pygame.event.get():

                if event.type == pygame.QUIT:
                    sys.exit()

                if event.type == pygame.KEYDOWN:

                    if event.key == pygame.K_ESCAPE:
                        return Cell(-1,-1,-1)

                    if event.key == pygame.K_RIGHT:
                        if coord[0] < (self.dimension - 1):
                            coord[0] += 1

                    if event.key == pygame.K_LEFT:
                        if coord[0] > 0:
                            coord[0] -= 1

                    if event.key == pygame.K_DOWN:
                        if coord[1] == (self.dimension - 1) and coord[2] < (self.dimension - 1):
                            coord[1] = 0
                            coord[2] += 1
                        elif coord[1] < (self.dimension - 1):
                            coord[1] += 1

                    if event.key == pygame.K_UP:
                        if coord[1] == 0 and coord[2] > 0:
                            coord[1] = self.dimension - 1
                            coord[2] -= 1
                        elif coord[1] > 0:
                            coord[1] -= 1

                    if event.key == pygame.K_RETURN:
                        pressedEnter = True

                    # Clear the dot that indicates position
                    for i in range(0, self.dimension):
                        for j in range(0, self.dimension):
                            for k in range(0, self.dimension):
                                coordAux = [i,j,k]
                                self.draw_move(coordAux, player_id, False, True)
                                pygame.display.update()
                                    
            # Board and Board Displayer use different coordinate systems.
            if pressedEnter:
                if not board.cell_empty(Cell(coord[2], coord[1], coord[0])):
                    pressedEnter = False
                else:
                    return Cell(coord[2], coord[1], coord[0])

    def avoid_not_responding(self):
        pygame.event.pump()
