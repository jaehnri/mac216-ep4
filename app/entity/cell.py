class Cell:
    """
    3D cell that follows the following coordinate system. The smallest
    @board.py unit. 

    X stands for which of the 4 grids, 0 being the most "upper" one;
    Y stands for which line of the chosen board;
    Z stands for which column of the chosen board;
    """
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z
