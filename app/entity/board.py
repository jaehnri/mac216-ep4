from random import randint
from entity.cell import Cell

class Board:
    """ 
    Class responsible for carrying a 3D integer array. Each cell
    has the ID of the player owning that cell. If no one owns that cell,
    we mark it with a '0'.
    """
    
    """ 
    The default cell has value '0'. This way, to indicate no player owns
    a given cell, you should check if that cell is 0.
    """
    EMPTY_CELL = 0

    """
    As this board is sized 4x4x4, the grid's value is four. This means that
    there are 4 different grids, each one having 4 lines and 4 columns.
    """
    GRID_SIZE = 4
    MAX_MOVES = 4*4*4

    def __init__(self):
        """ Initializes a new empty board. """
        
        self.grid = [[[Board.EMPTY_CELL for _ in range(Board.GRID_SIZE)] for _ in range(Board.GRID_SIZE)] for _ in range(Board.GRID_SIZE)]

    def cell_empty(self, cell):
        """ Boolean function that tells whether a cell is empty or not """

        return self.grid[cell.x][cell.y][cell.z] == Board.EMPTY_CELL

    def cell_available(self):
        """ Boolean function that returns true if at least one cell is empty"""

        for x in range(self.GRID_SIZE):
            for y in range(self.GRID_SIZE):
                for z in range(self.GRID_SIZE):
                    current_cell = Cell(x, y, z)
                    if self.cell_empty(current_cell):
                        return True
        return False

    def get_free_coord(self):
        for x in range(self.GRID_SIZE):
            for y in range(self.GRID_SIZE):
                for z in range(self.GRID_SIZE):
                    if self.cell_empty(Cell(x,y,z)):
                        return [z,y,x]
        return [0,0,0]

    @staticmethod
    def cell_in_grid(cell):
        """ Boolean function that returns true only if cell is inside the grid """

        return 0 <= cell.x <= Board.GRID_SIZE - 1 and 0 <= cell.y <= Board.GRID_SIZE - 1 and 0 <= cell.z <= Board.GRID_SIZE - 1

    @staticmethod
    def sort_cell():
        """ Returns a random Cell inside the global grid size (4x4x4) """

        x, y, z = randint(0, Board.GRID_SIZE - 1), randint(0, Board.GRID_SIZE - 1), randint(0, Board.GRID_SIZE - 1)
        return Cell(x, y, z) 

    @staticmethod
    def cell_out_of_grid(cell):
        """ Boolean function that returns true when cell is outside the grid """

        return not Board.cell_in_grid(cell)
