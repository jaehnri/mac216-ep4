import os
from entity.cell import Cell
from entity.board import Board
from use_case.board_inserter import BoardInserter
from gateway.board_displayer import BoardDisplay
from exception.exception import NoCellAvailableException

class PlayerStrategy:
    """
    'Interface' to help create a Strategy design pattern for
    players that differ only behaviourally.

    This way, outside this class, we can call a player function 
    such as choose_cell independently of its Object. The extern world
    doesn't need which player type the Object is to interact with it. 
    """
    def __init__(self, id):
        self.id = id
        self.board_inserter = BoardInserter()

    def choose_cell(self, board):
        pass

class HumanPlayer(PlayerStrategy):
    """
    This kind of player will be able to select a cell using some
    kind of user input. In this specific case, this happens using the
    pygame screen and the keyboard [KEYS].
    """
    
    def __init__(self, id):
        super(HumanPlayer, self).__init__(id)

    def get_player_input(self, board, board_displayer):
        """ 
        Allows the player to navigate through the board interface
        with its keyboard [KEYS].
        """

        return board_displayer.get_player_choice(self.id, board)

    def choose_cell(self, board, board_displayer):
        """
        After receiving the player input, put the player's ID in the board
        and draw it in the interface board.
        """

        chosen_cell = self.get_player_input(board, board_displayer)

        self.board_inserter.put_player(board, self.id, chosen_cell)
        board_displayer.draw_move([chosen_cell.z, chosen_cell.y, chosen_cell.x], self.id)

        return chosen_cell

class RawEaterPlayer(PlayerStrategy):
    """
    This kind os player will always choose the first cell available in the
    board. It navigates using a coordinate system where:

    X stands for which of the 4 grids, 0 being the most "upper" one;
    Y stands for which line of the chosen board;
    Z stands for which column of the chosen board;
    """
    def __init__(self, id):
        super(RawEaterPlayer, self).__init__(id)

    def choose_cell(self, board, board_displayer):
        """
        Will sequentially navigate the board searching for an empty cell.
        In case there are no empty cells left, will raise a NoCellAvailableException.
        """
        for x in range(board.GRID_SIZE):
            for y in range(board.GRID_SIZE):
                for z in range(board.GRID_SIZE):
                    chosen_cell = Cell(x, y, z)
                    if board.cell_empty(chosen_cell):
                        self.board_inserter.put_player(board, self.id, chosen_cell)
                        board_displayer.draw_move([chosen_cell.z, chosen_cell.y, chosen_cell.x], self.id)
                        board_displayer.avoid_not_responding()

                        return chosen_cell

        raise NoCellAvailableException("There are no cells available anymore. End the game.")

class ConfusedPlayer(PlayerStrategy):
    """
    This type of player will always choose a random cell in the board
    until find an empty cell. Before choosing a random cell, it will
    always ask the board if there actually are any empty cells left.

    In case there are no cells available anymore, will raise a
    NoCellAvailableException.
    """
    def __init__(self, id):
        super(ConfusedPlayer, self).__init__(id)

    def choose_cell(self, board, board_displayer):
        if board.cell_available():
            while True:
                sorted_cell = Board.sort_cell()

                if board.cell_empty(sorted_cell):
                    self.board_inserter.put_player(board, self.id, sorted_cell)
                    board_displayer.draw_move([sorted_cell.z, sorted_cell.y, sorted_cell.x], self.id)
                    board_displayer.avoid_not_responding()

                    return sorted_cell

        raise NoCellAvailableException("There are no cells available anymore. End the game.")
