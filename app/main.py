from gateway.board_displayer import BoardDisplay
from gateway.menu_displayer import MenuDisplay
from use_case.player_selector import PlayerSelector
from entity.player import HumanPlayer
from entity.player import RawEaterPlayer
from entity.player import ConfusedPlayer
from entity.board import Board
from use_case.paths_checker import WinnerTeller
import pygame, random, sys


def main_thread(menu, start_game):

    board = Board()
    board_displayer = BoardDisplay()
    winner_checker = WinnerTeller()
    moves = 0
    winner = False
    winner_id = 0

    if not start_game:
        menu.main_menu()
        player1_type, player2_type = MenuDisplay.start_menu(menu)
    else: 
        # Keep the same type from the last round if the player wants to restart
        player1_type = menu.player1
        player2_type = menu.player2

    player1 = PlayerSelector.get_player_type(1, player1_type)
    player2 = PlayerSelector.get_player_type(2, player2_type)

    board_displayer.reset_screen()
    board_displayer.draw_board()

    while (not winner and moves < Board.MAX_MOVES):
        pygame.time.Clock().tick(60)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    return -1

        board_displayer.update_player_display(player1.id)
        try:
            player1_chosen_cell = player1.choose_cell(board, board_displayer)
        except:
            return -1
        
        x = player1_chosen_cell.x
        y = player1_chosen_cell.y
        z = player1_chosen_cell.z

        if winner_checker.check_winner(board,x,y,z)[0] == player1.id:
            winner = True
            winner_id = player1.id

        if not winner:

            board_displayer.update_player_display(player2.id)
            try:
                player2_chosen_cell = player2.choose_cell(board, board_displayer)
            except:
                return -1

            x=player2_chosen_cell.x
            y=player2_chosen_cell.y
            z=player2_chosen_cell.z

            if winner_checker.check_winner(board,x,y,z)[0] == player2.id:
                winner = True
                winner_id = player2.id

            moves+=2

    if moves >= Board.MAX_MOVES:
        winner_id = 0

    if (winner_id != 0):
        for coord in winner_checker.check_winner(board,x,y,z)[1]:
            coordinates = [coord[2],coord[1],coord[0]] # Different coordinate systems
            board_displayer.highlight_move(coordinates,winner_id)

    return winner_id

def main():
    menu = MenuDisplay()
    start_game = False

    while True:
        winner_id = main_thread(menu, start_game)
        if winner_id == -1:
            start_game = False
            menu.board.reset_screen()
            winner_id = main_thread(menu, start_game)

        menu.winner_screen(winner_id)
        pygame.display.update()

        keyPress = False
        while not keyPress:

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()

                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        keyPress = True
                        start_game = False
                        menu.board.reset_screen()

                    elif event.key == pygame.K_RETURN:
                        keyPress = True
                        start_game = True
                        

if __name__ == "__main__":
    main()
