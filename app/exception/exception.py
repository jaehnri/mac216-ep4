class PlayerAlreadyPlacedException(Exception):
    """Raised when the player chooses a cell that a player already own"""
    pass

class OutOfBoardException(Exception):
    """Raised when the player chooses a cell that is out of the board grid"""
    pass

class NoCellAvailableException(Exception):
    """ Raised when there are no available cells in the board anymore """
    pass
