class CompletePathsChecker:

    def check_column_x_axis(self,board,x,y,z):
        column_complete=True

            #looks for complete paths in front of the x,y,z position parameter in the x_axis
        row=x+1
        while row<board.GRID_SIZE and column_complete:
            if board.grid[row][y][z] != board.grid[x][y][z]:
                column_complete=False
            row+=1

            #looks for complete paths in the back of the x,y,z position parameter in the x_axis
        row=x-1
        while row>-1 and column_complete:
            if board.grid[row][y][z] != board.grid[x][y][z] :
                column_complete=False
            row-=1

        return column_complete


    def check_row_y_axis(self,board,x,y,z):
        row_complete=True

         #looks for complete paths in front of the x,y,z position parameter in the y_axis
        layer=y+1
        while layer<board.GRID_SIZE and row_complete:
            if board.grid[x][layer][z] != board.grid[x][y][z]:
                row_complete=False
            layer+=1

         #looks for complete paths in the back of the x,y,z position parameter in the y_axis
        layer=y-1
        while layer>-1 and row_complete:
            if board.grid[x][layer][z] != board.grid[x][y][z] :
                row_complete=False
            layer-=1

        return row_complete

    def check_row_z_axis(self,board,x,y,z):
        row_complete=True

        row=z+1
        while row<board.GRID_SIZE and row_complete:
            if board.grid[x][y][row] != board.grid[x][y][z]:
                row_complete=False
            row+=1

         #looks for complete paths in the back of the x,y,z position parameter in the z_axis
        row=z-1
        while row>-1 and row_complete:
            if board.grid[x][y][row] != board.grid[x][y][z] :
                row_complete=False
            row-=1

        return row_complete

    def check_x_y_diagonal(self,board,x,y,z):
        if(y==x or (y+x==3)):
            diagonal_complete=True
            edge_row=x+1

            if(x==y):
                edge_layer=y+1
                while diagonal_complete and edge_row<board.GRID_SIZE and edge_layer<board.GRID_SIZE :
                    if board.grid[edge_row][edge_layer][z] !=  board.grid[x][y][z]:
                        diagonal_complete=False
                    edge_row+=1
                    edge_layer+=1

                edge_row=x-1
                edge_layer=y-1
                while diagonal_complete and edge_row>-1 and edge_layer>-1 :
                    if board.grid[edge_row][edge_layer][z] !=  board.grid[x][y][z]:
                        diagonal_complete=False
                    edge_row-=1
                    edge_layer-=1
            else:
                edge_layer=y-1
                while diagonal_complete and edge_row<board.GRID_SIZE and edge_layer>-1:
                    if board.grid[edge_row][edge_layer][z] !=  board.grid[x][y][z]:
                        diagonal_complete=False
                    edge_row+=1
                    edge_layer-=1

                edge_row=x-1
                edge_layer=y+1
                while diagonal_complete and edge_row>-1 and edge_layer<board.GRID_SIZE:
                    if board.grid[edge_row][edge_layer][z] !=  board.grid[x][y][z]:
                        diagonal_complete=False
                    edge_row-=1
                    edge_layer+=1


            return diagonal_complete
        else:
            return False

    def check_x_z_diagonal(self,board,x,y,z):

        if(z==x or (x+z==3)):
            diagonal_complete=True
            edge_depth=z+1

            if(x==z):
                edge_row=x+1

                while diagonal_complete and edge_row<board.GRID_SIZE and edge_depth<board.GRID_SIZE :
                    if board.grid[edge_row][y][edge_depth] !=  board.grid[x][y][z]:
                        diagonal_complete=False
                    edge_row+=1
                    edge_depth+=1

                edge_row=x-1
                edge_depth=z-1
                while diagonal_complete and edge_row>-1 and edge_depth>-1 :
                    if board.grid[edge_row][y][edge_depth] !=  board.grid[x][y][z]:
                        diagonal_complete=False
                    edge_row-=1
                    edge_depth-=1
            else:
                edge_row=x-1

                while diagonal_complete and edge_row>-1 and edge_depth<board.GRID_SIZE :
                    if board.grid[edge_row][y][edge_depth] !=  board.grid[x][y][z]:
                        diagonal_complete=False
                    edge_row-=1
                    edge_depth+=1

                edge_row=x+1
                edge_depth=z-1
                while diagonal_complete and edge_row<board.GRID_SIZE and edge_depth>-1 :
                    if board.grid[edge_row][y][edge_depth] !=  board.grid[x][y][z]:
                        diagonal_complete=False
                    edge_row+=1
                    edge_depth-=1


            return diagonal_complete
        else:
            return False

    def check_y_z_diagonal(self,board,x,y,z):

        if (z==y or (z+y==3)):
            diagonal_complete=True
            edge_depth=z+1

            if(z==y):
                edge_layer=y+1

                while diagonal_complete and edge_layer<board.GRID_SIZE and edge_depth<board.GRID_SIZE :
                    if board.grid[x][edge_layer][edge_depth] !=  board.grid[x][y][z]:
                        diagonal_complete=False
                    edge_layer+=1
                    edge_depth+=1

                edge_layer=y-1
                edge_depth=z-1
                while diagonal_complete and edge_layer>-1 and edge_depth>-1 :
                    if board.grid[x][edge_layer][edge_depth] !=  board.grid[x][y][z]:
                        diagonal_complete=False
                    edge_layer-=1
                    edge_depth-=1
            else:
                edge_layer=y-1
                while diagonal_complete and edge_layer>-1 and edge_depth<board.GRID_SIZE :
                    if board.grid[x][edge_layer][edge_depth] !=  board.grid[x][y][z]:
                        diagonal_complete=False
                    edge_layer-=1
                    edge_depth+=1

                edge_layer=y+1
                edge_depth=z-1
                while diagonal_complete and edge_layer<board.GRID_SIZE and edge_depth>-1 :
                    if board.grid[x][edge_layer][edge_depth] !=  board.grid[x][y][z]:
                        diagonal_complete=False
                    edge_layer+=1
                    edge_depth-=1


            return diagonal_complete
        else:
            return False


    #the following methods check for whether there is any colateral diagonal (in relation to the x-y, x-z and y-z planes) complete in the board.
    #as there are 4 possible diagonals in the board, there is 1 method for checking each one of these diagonals.
    #the vectorial reference used to orient the board was the one with left-handed orientation:
    # x-axis grows down, y-axis grows right and z-axis grows back relatively to the reader.

    def check_colateral_diagonal_with_upper_right_back_edge(self,board,x,y,z):
        #to check whether a colateral diagonal is complete, the vector (-1,1,1) is added to the
        #position (x,y,z) repeateadly until the upper-back-right edge of the board is reached.
        #Then, the same vector is now subtracted to get to the lower-front-left of the board.

        possibilities=[(3,3,0),(2,2,1),(1,1,2),(0,0,3)]
        coord=(x,y,z)

        if coord in possibilities:
            colateral_diagonal_complete=True
            row=x-1
            layer=y-1
            depth=z+1
            grid_size=board.GRID_SIZE
            while colateral_diagonal_complete and row>-1 and layer>-1 and depth<grid_size:
                if board.grid[row][layer][depth] != board.grid[x][y][z]:
                    colateral_diagonal_complete= False
                row-=1
                layer-=1
                depth+=1

            row=x+1
            layer=y+1
            depth=z-1
            while colateral_diagonal_complete and row<grid_size and layer<grid_size and depth>-1:
                if board.grid[row][layer][depth] != board.grid[x][y][z]:
                    colateral_diagonal_complete= False
                row+=1
                layer+=1
                depth-=1

            return colateral_diagonal_complete
        else:
            return False

    def check_colateral_diagonal_with_upper_right_front_edge(self,board,x,y,z):
        #same logic as the method above, but now the added vector is (-1,1,-1)

        possibilities=[(3,0,0),(2,1,1),(1,2,2),(0,3,3)]
        coord=(x,y,z)

        if coord in possibilities:
            colateral_diagonal_complete=True

            row=x+1
            layer=y-1
            depth=z-1
            grid_size=board.GRID_SIZE

            while colateral_diagonal_complete and row<grid_size and layer>-1 and depth>-1:
                if board.grid[row][layer][depth] != board.grid[x][y][z]:
                    colateral_diagonal_complete= False
                row+=1
                layer-=1
                depth=-1

            row=x-1
            layer=y+1
            depth=z+1
            while colateral_diagonal_complete and row>-1 and layer<grid_size and depth<grid_size:
                if board.grid[row][layer][depth] != board.grid[x][y][z]:
                    colateral_diagonal_complete= False
                row-=1
                layer+=1
                depth+=1

            return colateral_diagonal_complete
        else:
            return False

    def check_colateral_diagonal_with_upper_left_back_edge(self,board,x,y,z):
        #same logic as the method above, but now the added vector is (-1,-1,1)
        possibilities=[(3,0,3),(2,1,2),(1,2,1),(0,3,0)]
        coord=(x,y,z)

        if coord in possibilities:
            colateral_diagonal_complete=True
            row=x-1
            layer=y+1
            depth=z-1
            grid_size=board.GRID_SIZE

            while colateral_diagonal_complete and row>-1 and depth>-1 and layer<grid_size:
                if board.grid[row][layer][depth] != board.grid[x][y][z]:
                    colateral_diagonal_complete= False
                row-=1
                layer+=1
                depth-=1

            row=x+1
            layer=y-1
            depth=z+1
            while colateral_diagonal_complete and row<grid_size and layer>-1 and depth<grid_size:
                if board.grid[row][layer][depth] != board.grid[x][y][z]:
                    colateral_diagonal_complete= False
                row+=1
                layer-=1
                depth+=1

            return colateral_diagonal_complete
        else:
            return False

    def check_colateral_diagonal_with_upper_left_front_edge(self,board,x,y,z):
        #same logic as the method above, but now the added vector (-1,-1,-1)

        possibilities=[(3,3,3),(2,2,2),(1,1,1),(0,0,0)]
        coord=(x,y,z)

        if coord in possibilities:
            colateral_diagonal_complete=True

            row=x-1
            layer=y-1
            depth=z-1
            grid_size=board.GRID_SIZE

            while colateral_diagonal_complete and row>-1 and layer>-1 and depth>-1:
                if board.grid[row][layer][depth] != board.grid[x][y][z]:
                    colateral_diagonal_complete= False
                row-=1
                layer-=1
                depth-=1

            row=x+1
            layer=y+1
            depth=z+1
            while colateral_diagonal_complete and row<grid_size and layer<grid_size and depth<grid_size:
                if board.grid[row][layer][depth] != board.grid[x][y][z]:
                    colateral_diagonal_complete= False
                row+=1
                layer+=1
                depth+=1

            return colateral_diagonal_complete
        else:
            return False


class MultipleCompletePathsChecker(CompletePathsChecker):

    def check_rows_and_columns(self,board,x,y,z):
        x_axis_column_complete=self.check_column_x_axis(board,x,y,z)
        y_axis_row_complete=self.check_row_y_axis(board,x,y,z)
        z_axis_row_complete=self.check_row_z_axis(board,x,y,z)

        return ( x_axis_column_complete, y_axis_row_complete, z_axis_row_complete )

    def check_non_colateral_diagonals(self,board,x,y,z):
        x_y_diagonal_complete=self.check_x_y_diagonal(board,x,y,z)
        x_z_diagonal_complete=self.check_x_z_diagonal(board,x,y,z)
        y_z_diagonal_complete=self.check_y_z_diagonal(board,x,y,z)

        return ( x_y_diagonal_complete, x_z_diagonal_complete, y_z_diagonal_complete )

    def check_colateral_diagonals(self,board,x,y,z):
        colateral_diagonal_with_upper_right_back_edge=self.check_colateral_diagonal_with_upper_right_back_edge(board,x,y,z)
        colateral_diagonal_with_upper_right_front_edge=self.check_colateral_diagonal_with_upper_right_front_edge(board,x,y,z)
        colateral_diagonal_with_upper_left_back_edge=self.check_colateral_diagonal_with_upper_left_back_edge(board,x,y,z)
        colateral_diagonal_with_upper_left_front_edge=self.check_colateral_diagonal_with_upper_left_front_edge(board,x,y,z)

        return ( colateral_diagonal_with_upper_right_back_edge, colateral_diagonal_with_upper_right_front_edge,
        colateral_diagonal_with_upper_left_back_edge, colateral_diagonal_with_upper_left_front_edge )

class WinnerTeller(MultipleCompletePathsChecker):

    def get_complete_straight_path(self,axis,x,y,z):
        path=[]
        if(axis==1):
            path=[(0,y,z),(1,y,z),(2,y,z),(3,y,z)]
        elif (axis==2):
            path=[(x,0,z),(x,1,z),(x,2,z),(x,3,z)]
        elif (axis==3):
            path=[(x,y,0),(x,y,1),(x,y,2),(x,y,3)]

        return path

    def get_complete_non_colateral_diagonal_path(self,axis,x,y,z):
        path=[]
        if(axis==1):
            if(x==y):
                path=[(0,0,z),(1,1,z),(2,2,z),(3,3,z)]
            else:
                path=[(0,3,z),(1,2,z),(2,1,z),(3,0,z)]
        elif(axis==2):
            if(x==z):
                path=[(0,y,0),(1,y,1),(2,y,2),(3,y,3)]
            else:
                path=[(3,y,0),(2,y,1),(1,y,2),(0,y,3)]
        elif(axis==3):
            if(y==z):
                path=[(x,0,0),(x,1,1),(x,2,2),(x,3,3)]
            else:
                path=[(x,3,0),(x,2,1),(x,1,2),(x,0,3)]

        return path

    def get_complete_colateral_diagonal_path(self,axis,x,y,z):
        path=[]
        if (axis==1):
            path=[(3,3,0),(2,2,1),(1,1,2),(0,0,3)]
        elif(axis==2):
            path=[(3,0,0),(2,1,1),(1,2,2),(0,3,3)]
        elif(axis==3):
            path=[(3,0,3),(2,1,2),(1,2,1),(0,3,0)]
        elif(axis==4):
            path=[(3,3,3),(2,2,2),(1,1,1),(0,0,0)]

        return path

    def check_winner(self,board,x,y,z):
        winner_path=[]
        win=[]
        axis=-1
        winner_id=-1
        straight_path_complete=self.check_rows_and_columns(board,x,y,z)
        non_colateral_diagonal_complete=self.check_non_colateral_diagonals(board,x,y,z)
        colateral_diagonal_complete=self.check_colateral_diagonals(board,x,y,z)

        for i in range(len(straight_path_complete)):
            if straight_path_complete[i]:
                axis=i+1
                winner_id=board.grid[x][y][z]
                winner_path=self.get_complete_straight_path(axis,x,y,z)
                win=[winner_id,winner_path]
                return win

        for j in range(len(non_colateral_diagonal_complete)):
            if non_colateral_diagonal_complete[j]:
                 axis=j+1
                 winner_id=board.grid[x][y][z]
                 winner_path=self.get_complete_non_colateral_diagonal_path(axis,x,y,z)
                 win=[winner_id,winner_path]
                 return win

        for k in range(len(colateral_diagonal_complete)):
            if colateral_diagonal_complete[k]:
                axis=k+1
                winner_id=board.grid[x][y][z]
                winner_path=self.get_complete_colateral_diagonal_path(axis,x,y,z)
                win=[winner_id,winner_path]
                return win

        win=[-1,[(-1,-1,-1),(-1,-1,-1),(-1,-1,-1),(-1,-1,-1,-1)]]
        return win
