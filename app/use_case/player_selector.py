from entity.player import HumanPlayer
from entity.player import RawEaterPlayer
from entity.player import ConfusedPlayer

class PlayerSelector:

    @staticmethod
    def get_player_type(player_id, player_type_id):
        """
        Given the player ID and a player type ID, returns a PlayerStrategy
        derived Object.
        """
        selector = {
            0: HumanPlayer(player_id),
            1: RawEaterPlayer(player_id),
            2: ConfusedPlayer(player_id)
        }

        return selector.get(player_type_id)
