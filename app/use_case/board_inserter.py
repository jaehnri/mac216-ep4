from entity.board import Board
from entity.cell import Cell
from exception.exception import PlayerAlreadyPlacedException
from exception.exception import OutOfBoardException

class BoardInserter:
    """
    Given a Board, a Player ID and a Cell, will try to put the Player ID in
    the Cell of the Board.

    In case the Cell does not fit in the Board, i.e., any coordinate is greater
    than the Board's size, will raise an OutOfBoardException.

    If the Cell does fit in the Board but there already is a Player ID there,
    will raise a PlayerAlreadyPlacedException
    """

    def put_player(self, board, player_id, cell):
        if board.cell_out_of_grid(cell):
            raise OutOfBoardException('Coordinates should be between 0 and 3')

        if board.cell_empty(cell):
            board.grid[cell.x][cell.y][cell.z] = player_id
            return
        else:
            raise PlayerAlreadyPlacedException('Player {} already at ({};{};{})'.format(board.grid[cell.x][cell.y][cell.z], cell.x, cell.y, cell.z))

class BoardEraser:
    """
    Test class to erase the Player ID of a given cell. If cell is already empty,
    nothing changes.
    """
    def erase(self, board, cell):
        board.grid[cell.x][cell.y][cell.z] = board.EMPTY_CELL
        return
