from use_case.paths_checker import MultipleCompletePathsChecker
from use_case.paths_checker import WinnerTeller
from entity.board import Board
from entity.cell import Cell
from use_case.player_selector import PlayerSelector
from use_case.board_inserter import BoardInserter
from use_case.board_inserter import BoardEraser
from copy import copy
import pytest

@pytest.fixture
def player_id():
    return 1

@pytest.fixture
def empty_board():
    return Board()

@pytest.fixture
def tester():
    return MultipleCompletePathsChecker()

@pytest.fixture
def filled_board(player_id,empty_board):
    filled_board = empty_board
    filled_board.grid = [[[player_id for _ in range(empty_board.GRID_SIZE)] for _ in range(empty_board.GRID_SIZE)] for _ in range(empty_board.GRID_SIZE)]
    return filled_board

def all_positions_of_the_board():
    _board=Board()
    positions=[]
    for row in range(len(_board.grid[0])):
        for column in range(len(_board.grid[1])):
            for layer in range(len(_board.grid[2])):
                cell=Cell(row,column,layer)
                positions.append(cell)

    return positions

cells=all_positions_of_the_board()

@pytest.mark.parametrize("cell",cells)
def test_all_possible_complete_paths(cell,filled_board,tester):
    x = cell.x
    y = cell.y
    z = cell.z
    coord=(x,y,z)
    colateral_possibilities1=[(3,3,0),(2,2,1),(1,1,2),(0,0,3)]
    colateral_possibilities2=[(3,0,0),(2,1,1),(1,2,2),(0,3,3)]
    colateral_possibilities3=[(3,0,3),(2,1,2),(1,2,1),(0,3,0)]
    colateral_possibilities4=[(3,3,3),(2,2,2),(1,1,1),(0,0,0)]
    expected_for_rows_and_columns=(True,True,True)
    expected_for_non_colateral_diagonals=[False,False,False]
    expected_for_colateral_diagonals=[False,False,False,False]

    if ((x+y+z==6 and min(x,y,z)==0) or (x+y+z==9) or (x==y==z==0)):
        expected_for_non_colateral_diagonals= [True,True,True]

    else:
        if(x==y or x+y==3):
            expected_for_non_colateral_diagonals[0]=True
        if(x==z or x+z==3):
            expected_for_non_colateral_diagonals[1]=True
        if(z==y or z+y==3):
            expected_for_non_colateral_diagonals[2]=True

    if coord in colateral_possibilities1:
        expected_for_colateral_diagonals[0]=True
    elif coord in colateral_possibilities2:
        expected_for_colateral_diagonals[1]=True
    elif coord in colateral_possibilities3:
        expected_for_colateral_diagonals[2]=True
    elif coord in colateral_possibilities4:
        expected_for_colateral_diagonals[3]=True

    expected_for_colateral_diagonals=tuple(expected_for_colateral_diagonals)
    expected_for_non_colateral_diagonals=tuple(expected_for_non_colateral_diagonals)

    assert tester.check_rows_and_columns(filled_board,x,y,z) == expected_for_rows_and_columns
    assert tester.check_non_colateral_diagonals(filled_board,x,y,z) == expected_for_non_colateral_diagonals
    assert tester.check_colateral_diagonals(filled_board,x,y,z) == expected_for_colateral_diagonals


def random_six_positions_filled_board():
    _board=Board()
    inserter=BoardInserter()
    player1 = PlayerSelector.get_player_type(1, 2)
    player2 = PlayerSelector.get_player_type(2, 2)
    for i in range(5):
        if(i%2==0):
            id=1
        else:
            id=2

        cell=Board.sort_cell()
        while not _board.cell_empty(cell):
            cell=Board.sort_cell()
        inserter.put_player(_board,id,cell)
    return _board

random_almost_filled_boards=[]
for i in range(100):
    random_almost_filled_boards.append(random_six_positions_filled_board())

@pytest.mark.parametrize("board",random_almost_filled_boards)
@pytest.mark.parametrize("cell",cells)
def test_should_return_no_winners_for_six_positions_filled_board(board,cell):
    checker=WinnerTeller()
    x=cell.x
    y=cell.y
    z=cell.z
    if not board.cell_empty(cell):
        assert checker.check_winner(board,x,y,z)==[-1,[(-1,-1,-1),(-1,-1,-1),(-1,-1,-1),(-1,-1,-1,-1)]]

def tie_board_generator():
    _board=Board()
    eraser=BoardEraser()
    inserter=BoardInserter()
    player1 = PlayerSelector.get_player_type(1, 2)
    player2 = PlayerSelector.get_player_type(2, 2)
    winner_checker=MultipleCompletePathsChecker()
    cell=Board.sort_cell()

    for i in range(64):
        next=False
        if(i%2==0):
            id=1
        else:
            id=2

        while not next:
            aux=copy(cell)

            if not _board.cell_available():
                _board=Board()
                cell=Board.sort_cell()
            else:
                cell=Board.sort_cell()
                eraser.erase(_board,aux)


            while not _board.cell_empty(cell):
                cell=Board.sort_cell()

            x=cell.x
            y=cell.y
            z=cell.z
            inserter.put_player(_board,id,cell)

            rows_and_columns_complete=winner_checker.check_rows_and_columns(_board,x,y,z)
            if True in rows_and_columns_complete:
                continue

            non_colateral_diagonal_complete=winner_checker.check_non_colateral_diagonals(_board,x,y,z)
            if True in non_colateral_diagonal_complete:
                continue

            colateral_diagonals_complete=winner_checker.check_colateral_diagonals(_board,x,y,z)

            if True in colateral_diagonals_complete:
                continue
            else:
                next=True

    return _board

random_tie_boards=[]

for i in range(100):
    random_tie_boards.append(tie_board_generator())


@pytest.mark.parametrize("board",random_tie_boards)
@pytest.mark.parametrize("cell",cells)
def test_should_return_no_winners_for_tie_board(board,cell):
    checker=WinnerTeller()
    x=cell.x
    y=cell.y
    z=cell.z
    if not board.cell_empty(cell):
        assert  checker.check_winner(board,x,y,z)==[-1,[(-1,-1,-1),(-1,-1,-1),(-1,-1,-1),(-1,-1,-1,-1)]]
