from entity.board import Board
from entity.cell import Cell
from use_case.board_inserter import BoardInserter
from exception.exception import PlayerAlreadyPlacedException
import pytest

@pytest.fixture
def player_id():
    return 1

@pytest.fixture
def empty_board():
    return Board()

@pytest.fixture
def filled_board():
    filled_board = Board()
    filled_board.grid = [[[player_id for _ in range(Board.GRID_SIZE)] for _ in range(Board.GRID_SIZE)] for _ in range(Board.GRID_SIZE)]
    return filled_board

def test_should_place_player_id_in_cell_when_cell_is_empty(player_id, empty_board):
    empty_cell = Cell(1, 1, 1)

    handler = BoardInserter()
    handler.put_player(empty_board, player_id, empty_cell)

    assert empty_board.grid[1][1][1] == player_id

def test_should_raise_exception_when_put_player_in_filled_board(player_id, filled_board):
    owned_cell = Cell(1, 1, 1)

    with pytest.raises(PlayerAlreadyPlacedException):
        handler = BoardInserter()
        handler.put_player(filled_board, player_id, owned_cell)
