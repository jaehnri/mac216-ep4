from entity.board import Board
from entity.cell import Cell
from use_case.board_inserter import BoardInserter
from exception.exception import PlayerAlreadyPlacedException
import pytest
from copy import copy

@pytest.fixture
def player_id():
    return 1

@pytest.fixture
def empty_board():
    return Board()

@pytest.fixture
def filled_board(player_id,empty_board):
    filled_board = empty_board
    filled_board.grid = [[[player_id for _ in range(empty_board.GRID_SIZE)] for _ in range(empty_board.GRID_SIZE)] for _ in range(empty_board.GRID_SIZE)]
    return filled_board

def all_positions_of_the_board():
    _board=Board()
    positions=[]
    for row in range(len(_board.grid[0])):
        for column in range(len(_board.grid[1])):
            for layer in range(len(_board.grid[2])):
                cell=Cell(row,column,layer)
                positions.append(cell)

    return positions

cells=all_positions_of_the_board()

@pytest.mark.parametrize("cell",cells)
def test_should_return_all_cells_empty(cell,empty_board):
    assert empty_board.cell_empty(cell) == True
    assert empty_board.cell_available() == True

@pytest.mark.parametrize("cell",cells)
def test_should_return_all_cells_filled(cell,filled_board):
    assert filled_board.cell_empty(cell) == False
    assert filled_board.cell_available()  == False

def filled_board2():
    filled_board2 = Board()
    filled_board2.grid = [[[1 for _ in range(filled_board2.GRID_SIZE)] for _ in range(filled_board2.GRID_SIZE)] for _ in range(filled_board2.GRID_SIZE)]
    return filled_board2

def almost_filled_board(cell):
    almost_filled_board=filled_board2()
    almost_filled_board.grid[cell.x][cell.y][cell.z]=0
    return almost_filled_board

almost_filled_boards=[]
for cell in cells:
    almost_filled_boards.append(almost_filled_board(cell))

@pytest.mark.parametrize("board",almost_filled_boards)
def test_should_return_theres_cell_available(board):
    assert board.cell_available() == True

@pytest.mark.parametrize("cell",cells)
def test_cell_in_grid(empty_board,cell):

    for first_bound in range(3):
        aux=copy(cell)
        if(first_bound == 0):
            aux.x = -1
        elif(first_bound == 1):
            aux.y = -1
        else:
            aux.z = -1

        assert Board.cell_in_grid(aux) == False
        assert Board.cell_out_of_grid(aux) == True

        for second_bound in range(3):
            if(second_bound == 0):
                aux.x = 4
            elif(second_bound == 1):
                aux.y = 4
            else:
                aux.z = 4

            assert Board.cell_in_grid(aux) == False
            assert Board.cell_out_of_grid(aux) == True

    aux=copy(cell)

    aux.x = aux.y = aux.z = -3
    assert Board.cell_out_of_grid(aux) == True
    assert Board.cell_in_grid(aux) == False

    aux.x = aux.y = aux.z= 10
    assert Board.cell_out_of_grid(aux) == True
    assert Board.cell_in_grid(aux) == False
